﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KarolMaliszewski
{
    class Data
    {
        private string _name;
        private string _surname;
        private string _address;
        private string _phone;

        public Data(string name, string surname, string address, string phone)
        {
            _name = name;
            _surname = surname;
            _address = address;
            _phone = phone;
        }

        public override string ToString()
        {
            string format = string.Format(" {0} {1} {2} {3} {4} {5} {6}", _name,Environment.NewLine ,_surname, Environment.NewLine, _address, Environment.NewLine, _phone);
            return format;
        }
    }
}
