﻿namespace KarolMaliszewski
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.NameBox = new System.Windows.Forms.TextBox();
            this.NameLabel = new System.Windows.Forms.Label();
            this.Next1button = new System.Windows.Forms.Button();
            this.SurnameLable = new System.Windows.Forms.Label();
            this.SurnameBox = new System.Windows.Forms.TextBox();
            this.AddressLabel = new System.Windows.Forms.Label();
            this.AddressBox = new System.Windows.Forms.TextBox();
            this.PhoneLabel = new System.Windows.Forms.Label();
            this.PhoneBox = new System.Windows.Forms.TextBox();
            this.SurnameNextButton = new System.Windows.Forms.Button();
            this.SurnamePrevButton = new System.Windows.Forms.Button();
            this.AddressPrevButton = new System.Windows.Forms.Button();
            this.AddressNextButton = new System.Windows.Forms.Button();
            this.PhoneEndButton = new System.Windows.Forms.Button();
            this.PhonePrevButton = new System.Windows.Forms.Button();
            this.Showlabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // NameBox
            // 
            this.NameBox.Location = new System.Drawing.Point(87, 64);
            this.NameBox.Name = "NameBox";
            this.NameBox.Size = new System.Drawing.Size(123, 20);
            this.NameBox.TabIndex = 0;
            // 
            // NameLabel
            // 
            this.NameLabel.AutoSize = true;
            this.NameLabel.Location = new System.Drawing.Point(84, 24);
            this.NameLabel.Name = "NameLabel";
            this.NameLabel.Size = new System.Drawing.Size(55, 13);
            this.NameLabel.TabIndex = 1;
            this.NameLabel.Text = "Podaj imie";
            // 
            // Next1button
            // 
            this.Next1button.Location = new System.Drawing.Point(216, 61);
            this.Next1button.Name = "Next1button";
            this.Next1button.Size = new System.Drawing.Size(75, 23);
            this.Next1button.TabIndex = 2;
            this.Next1button.Text = "Nastepna";
            this.Next1button.UseVisualStyleBackColor = true;
            this.Next1button.Click += new System.EventHandler(this.Next1button_Click);
            // 
            // SurnameLable
            // 
            this.SurnameLable.AutoSize = true;
            this.SurnameLable.Location = new System.Drawing.Point(84, 24);
            this.SurnameLable.Name = "SurnameLable";
            this.SurnameLable.Size = new System.Drawing.Size(81, 13);
            this.SurnameLable.TabIndex = 4;
            this.SurnameLable.Text = "Podaj nazwisko";
            this.SurnameLable.Visible = false;
            // 
            // SurnameBox
            // 
            this.SurnameBox.Location = new System.Drawing.Point(87, 64);
            this.SurnameBox.Name = "SurnameBox";
            this.SurnameBox.Size = new System.Drawing.Size(123, 20);
            this.SurnameBox.TabIndex = 3;
            this.SurnameBox.Visible = false;
            // 
            // AddressLabel
            // 
            this.AddressLabel.AutoSize = true;
            this.AddressLabel.Location = new System.Drawing.Point(84, 24);
            this.AddressLabel.Name = "AddressLabel";
            this.AddressLabel.Size = new System.Drawing.Size(63, 13);
            this.AddressLabel.TabIndex = 6;
            this.AddressLabel.Text = "Podaj adres";
            this.AddressLabel.Visible = false;
            // 
            // AddressBox
            // 
            this.AddressBox.Location = new System.Drawing.Point(87, 61);
            this.AddressBox.Name = "AddressBox";
            this.AddressBox.Size = new System.Drawing.Size(123, 20);
            this.AddressBox.TabIndex = 5;
            this.AddressBox.Visible = false;
            // 
            // PhoneLabel
            // 
            this.PhoneLabel.AutoSize = true;
            this.PhoneLabel.Location = new System.Drawing.Point(84, 24);
            this.PhoneLabel.Name = "PhoneLabel";
            this.PhoneLabel.Size = new System.Drawing.Size(107, 13);
            this.PhoneLabel.TabIndex = 8;
            this.PhoneLabel.Text = "Podaj numer telefonu";
            this.PhoneLabel.Visible = false;
            // 
            // PhoneBox
            // 
            this.PhoneBox.Location = new System.Drawing.Point(87, 61);
            this.PhoneBox.Name = "PhoneBox";
            this.PhoneBox.Size = new System.Drawing.Size(123, 20);
            this.PhoneBox.TabIndex = 7;
            this.PhoneBox.Visible = false;
            // 
            // SurnameNextButton
            // 
            this.SurnameNextButton.Location = new System.Drawing.Point(216, 61);
            this.SurnameNextButton.Name = "SurnameNextButton";
            this.SurnameNextButton.Size = new System.Drawing.Size(75, 23);
            this.SurnameNextButton.TabIndex = 9;
            this.SurnameNextButton.Text = "Nastepna";
            this.SurnameNextButton.UseVisualStyleBackColor = true;
            this.SurnameNextButton.Visible = false;
            this.SurnameNextButton.Click += new System.EventHandler(this.SurnameNextButton_Click);
            // 
            // SurnamePrevButton
            // 
            this.SurnamePrevButton.Location = new System.Drawing.Point(6, 61);
            this.SurnamePrevButton.Name = "SurnamePrevButton";
            this.SurnamePrevButton.Size = new System.Drawing.Size(75, 23);
            this.SurnamePrevButton.TabIndex = 10;
            this.SurnamePrevButton.Text = "Poprzedni";
            this.SurnamePrevButton.UseVisualStyleBackColor = true;
            this.SurnamePrevButton.Visible = false;
            this.SurnamePrevButton.Click += new System.EventHandler(this.SurnamePrevButton_Click);
            // 
            // AddressPrevButton
            // 
            this.AddressPrevButton.Location = new System.Drawing.Point(6, 61);
            this.AddressPrevButton.Name = "AddressPrevButton";
            this.AddressPrevButton.Size = new System.Drawing.Size(75, 23);
            this.AddressPrevButton.TabIndex = 11;
            this.AddressPrevButton.Text = "Poprzedni";
            this.AddressPrevButton.UseVisualStyleBackColor = true;
            this.AddressPrevButton.Visible = false;
            this.AddressPrevButton.Click += new System.EventHandler(this.AddressPrevButton_Click);
            // 
            // AddressNextButton
            // 
            this.AddressNextButton.Location = new System.Drawing.Point(216, 61);
            this.AddressNextButton.Name = "AddressNextButton";
            this.AddressNextButton.Size = new System.Drawing.Size(75, 23);
            this.AddressNextButton.TabIndex = 12;
            this.AddressNextButton.Text = "Nastepna";
            this.AddressNextButton.UseVisualStyleBackColor = true;
            this.AddressNextButton.Visible = false;
            this.AddressNextButton.Click += new System.EventHandler(this.AddressNextButton_Click);
            // 
            // PhoneEndButton
            // 
            this.PhoneEndButton.Location = new System.Drawing.Point(216, 62);
            this.PhoneEndButton.Name = "PhoneEndButton";
            this.PhoneEndButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.PhoneEndButton.Size = new System.Drawing.Size(75, 23);
            this.PhoneEndButton.TabIndex = 13;
            this.PhoneEndButton.Text = "Zakoncz";
            this.PhoneEndButton.UseVisualStyleBackColor = true;
            this.PhoneEndButton.Visible = false;
            this.PhoneEndButton.Click += new System.EventHandler(this.PhoneEndButton_Click);
            // 
            // PhonePrevButton
            // 
            this.PhonePrevButton.Location = new System.Drawing.Point(6, 62);
            this.PhonePrevButton.Name = "PhonePrevButton";
            this.PhonePrevButton.Size = new System.Drawing.Size(75, 23);
            this.PhonePrevButton.TabIndex = 14;
            this.PhonePrevButton.Text = "Poprzedni";
            this.PhonePrevButton.UseVisualStyleBackColor = true;
            this.PhonePrevButton.Visible = false;
            this.PhonePrevButton.Click += new System.EventHandler(this.PhonePrevButton_Click);
            // 
            // Showlabel
            // 
            this.Showlabel.Location = new System.Drawing.Point(84, 102);
            this.Showlabel.Name = "Showlabel";
            this.Showlabel.Size = new System.Drawing.Size(126, 82);
            this.Showlabel.TabIndex = 15;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(311, 208);
            this.Controls.Add(this.Showlabel);
            this.Controls.Add(this.PhonePrevButton);
            this.Controls.Add(this.PhoneEndButton);
            this.Controls.Add(this.AddressNextButton);
            this.Controls.Add(this.AddressPrevButton);
            this.Controls.Add(this.SurnamePrevButton);
            this.Controls.Add(this.SurnameNextButton);
            this.Controls.Add(this.PhoneLabel);
            this.Controls.Add(this.PhoneBox);
            this.Controls.Add(this.AddressLabel);
            this.Controls.Add(this.AddressBox);
            this.Controls.Add(this.SurnameLable);
            this.Controls.Add(this.SurnameBox);
            this.Controls.Add(this.Next1button);
            this.Controls.Add(this.NameLabel);
            this.Controls.Add(this.NameBox);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox NameBox;
        private System.Windows.Forms.Label NameLabel;
        private System.Windows.Forms.Button Next1button;
        private System.Windows.Forms.Label SurnameLable;
        private System.Windows.Forms.TextBox SurnameBox;
        private System.Windows.Forms.Label AddressLabel;
        private System.Windows.Forms.TextBox AddressBox;
        private System.Windows.Forms.Label PhoneLabel;
        private System.Windows.Forms.TextBox PhoneBox;
        private System.Windows.Forms.Button SurnameNextButton;
        private System.Windows.Forms.Button SurnamePrevButton;
        private System.Windows.Forms.Button AddressPrevButton;
        private System.Windows.Forms.Button AddressNextButton;
        private System.Windows.Forms.Button PhoneEndButton;
        private System.Windows.Forms.Button PhonePrevButton;
        private System.Windows.Forms.Label Showlabel;
    }
}

