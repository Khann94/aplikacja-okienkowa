﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KarolMaliszewski
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Next1button_Click(object sender, EventArgs e)
        {
            NameLabel.Visible = false;
            NameBox.Visible = false;
            Next1button.Visible = false;
            SurnameBox.Visible = true;
            SurnameLable.Visible = true;
            SurnameNextButton.Visible = true;
            SurnamePrevButton.Visible = true;
        }

        private void SurnamePrevButton_Click(object sender, EventArgs e)
        {
            NameLabel.Visible = true;
            NameBox.Visible = true;
            Next1button.Visible = true;
            SurnameBox.Visible = false;
            SurnameLable.Visible = false;
            SurnameNextButton.Visible = false;
            SurnamePrevButton.Visible = false;
        }

        private void SurnameNextButton_Click(object sender, EventArgs e)
        {
            SurnameBox.Visible = false;
            SurnameLable.Visible = false;
            SurnameNextButton.Visible = false;
            SurnamePrevButton.Visible = false;
            AddressLabel.Visible = true;
            AddressBox.Visible = true;
            AddressNextButton.Visible = true;
            AddressPrevButton.Visible = true;
        }

        private void AddressPrevButton_Click(object sender, EventArgs e)
        {
            SurnameBox.Visible = true;
            SurnameLable.Visible = true;
            SurnameNextButton.Visible = true;
            SurnamePrevButton.Visible = true;
            AddressLabel.Visible = false;
            AddressBox.Visible = false;
            AddressNextButton.Visible = false;
            AddressPrevButton.Visible = false;
        }

        private void AddressNextButton_Click(object sender, EventArgs e)
        {
            AddressLabel.Visible = false;
            AddressBox.Visible = false;
            AddressNextButton.Visible = false;
            AddressPrevButton.Visible = false;
            PhoneBox.Visible = true;
            PhoneLabel.Visible = true;
            PhoneEndButton.Visible = true;
            PhonePrevButton.Visible = true;
        }

        private void PhonePrevButton_Click(object sender, EventArgs e)
        {
            AddressLabel.Visible = true;
            AddressBox.Visible = true;
            AddressNextButton.Visible = true;
            AddressPrevButton.Visible = true;
            PhoneBox.Visible = false;
            PhoneLabel.Visible = false;
            PhoneEndButton.Visible = false;
            PhonePrevButton.Visible = false;
            Showlabel.Visible = false;
        }

        private void PhoneEndButton_Click(object sender, EventArgs e)
        {
            Data data = new Data(NameBox.Text, SurnameBox.Text, AddressBox.Text, PhoneBox.Text);
            Showlabel.Text = data.ToString();
            Showlabel.Visible = true;
        }
    }
}
